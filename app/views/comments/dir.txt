Assignment No4 - 
 1) modify show.html.erb to support Ajax calls (XHR)
    w/ 'remote: true' added to 'form'

 2) add the partial template: _comment.html.erb in views/comments/..
    and get code we need from: /views/posts/show.html.erb

 3) modify controller to respond to Ajax request:
    'comments_controller.rb' w/ 'respond_to format.js'

 4) create JavaScript code that will be returned and be dynamically 
    executed in the web page 'create.js.erb'

